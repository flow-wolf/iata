# IATA
All airport IATA codes and coordinates in the following format:
```
    "ABV": {
        "city": "Abuja",
        "country": "Nigeria",
        "iata_code": "ABV",
        "latitude": "9.0067901611",
        "longitude": "7.2631697655",
        "name": "Nnamdi Azikiwe International Airport"
    },
```